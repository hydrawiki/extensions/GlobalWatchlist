CREATE TABLE IF NOT EXISTS /*_*/gwl_watchlist (
`wid` int(14) NOT NULL,
  `global_id` int(14) NOT NULL,
  `site_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `list` mediumblob NOT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/gwl_watchlist ADD PRIMARY KEY (`wid`), ADD KEY `global_id_site_key` (`global_id`,`site_key`);

ALTER TABLE /*_*/gwl_watchlist MODIFY `wid` int(14) NOT NULL AUTO_INCREMENT;