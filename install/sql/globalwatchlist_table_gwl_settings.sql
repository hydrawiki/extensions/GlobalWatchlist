CREATE TABLE /*_*/gwl_settings (
  `sid` int(14) NOT NULL,
  `global_id` int(14) NOT NULL,
  `site_keys` mediumblob NOT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/gwl_settings ADD PRIMARY KEY (`sid`), ADD KEY `global_id` (`global_id`);

ALTER TABLE /*_*/gwl_settings MODIFY `sid` int(14) NOT NULL AUTO_INCREMENT;