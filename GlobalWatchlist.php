<?php
/**
 * GlobalWatchlist
 * GlobalWatchlist Mediawiki Settings
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2016 Curse Inc.
 * @license		GPLv3
 * @package		GlobalWatchlist
 * @link		https://github.com/HydraWiki/GlobalWatchlist
 *
 **/

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'GlobalWatchlist' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['GlobalWatchlist'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for GlobalWatchlist extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
 } else {
	die( 'This version of the GlobalWatchlist extension requires MediaWiki 1.25+' );
}
